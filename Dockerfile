# ベースとなる親のDockerfileを指定。
# Docker Hubで提供されているOfficial Repositoryから、CentOS6.8をベースにして構築する。
# https://hub.docker.com/_/centos/
FROM centos:7

MAINTAINER Nakamura Naoto <nakamura.naoto@moviemarket.co.jp>
MAINTAINER Senoue Hiromasa <senoue.hiromasa@moviemarket.co.jp>

# 作業ディレクトリを作成。
WORKDIR /root/.tmp

### YUMの設定 ############################################################

# YUMの設定をおこなう。
# EPEL,REMIのリポジトリをインストールする。
RUN yum -y install epel-release
RUN rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-7.rpm

### インストール #########################################################

# Go, Apache, Crontab, Git, phpMyAdminのインストール
RUN yum -y install golang httpd crontabs git

# MySQLのインストール
RUN yum -y install mariadb mariadb-server

# PHPをインストール
RUN yum -y install --enablerepo=remi,remi-php71 php php-devel php-mbstring php-pdo php-gd php-xml php-mcrypt phpMyAdmin

### 設定をして起動 #######################################################

# Cronを設定して起動
RUN echo '0 * * * * root GOPATH="/data/doc_root/service/mireru" /bin/bash -l -c /data/doc_root/service/mireru/bin/mireru >> /var/log/cron.log 2>&1' >> /etc/crontab
RUN systemctl enable crond

# Apacheを設定して起動
RUN touch /etc/httpd/conf.d/local-feed_moviemarket_jp.conf
RUN echo $'<VirtualHost *:80>\n\
    DocumentRoot /data/doc_root/service/mireru/feed\n\
    ServerName local.mireru.movie\n\
    ErrorLog logs/local.mireru.movie-error_log\n\
    CustomLog logs/local.mireru.movie-access_log common\n\
    <Directory "/data/doc_root/service/mireru">\n\
        AllowOverride All\n\
    </Directory>\n\
    \n\
    # phpMyAdmin\n\
    Alias /pma /usr/share/phpMyAdmin\n\
    <Directory "/usr/share/phpMyAdmin/">\n\
        AllowOverride All\n\
        AddDefaultCharset UTF-8\n\
        Require all granted\n\
        DirectoryIndex index.php\n\
        AddType application/x-httpd-php .php\n\
        <FilesMatch \.php$>\n\
            SetHandler application/x-httpd-php\n\
        </FilesMatch>\n\
    </Directory>\n\
    <Directory "/usr/share/phpMyAdmin/setup/">\n\
        Require all granted\n\
    </Directory>\n\
    <Directory "/usr/share/phpMyAdmin/libraries/">\n\
        Require all denied\n\
    </Directory>\n\
    <Directory "/usr/share/phpMyAdmin/setup/lib/">\n\
        Require all denied\n\
    </Directory>\n\
    <Directory "/usr/share/phpMyAdmin/setup/frames/">\n\
        Require all denied\n\
    </Directory>\n\
</VirtualHost>' >> /etc/httpd/conf.d/local-mireru_movie.conf
RUN systemctl enable httpd

# phpMyAdminの設定
RUN echo $'$cfg["Servers"][$i]["auth_type"] = "config";\n\
$cfg["Servers"][$i]["user"] = "root";\n\
$cfg["Servers"][$i]["password"] = "";\n\
$cfg["Servers"][$i]["AllowRoot"] = true;\n\
$cfg["Servers"][$i]["nopassword"] = true;\n\
$cfg["Servers"][$i]["AllowNoPassword"] = true;\n\
$cfg["Servers"][$i]["AllowNoPasswordRoot"] = true;\n\
$cfg["MaxRows"] = 50;' >> /etc/phpMyAdmin/config.inc.php

RUN echo $'php_value max_input_vars 10000' > /usr/share/phpMyAdmin/.htaccess

# MySQL起動
RUN systemctl enable mariadb

WORKDIR /data/doc_root/service/mireru

EXPOSE 80

# DOCKER RUN実行時に/sbin/initが実行されるようにする。
ENV GOPATH /data/doc_root/service/mireru
ENV PATH /data/doc_root/service/mireru/bin:$PATH

ENTRYPOINT /sbin/init
